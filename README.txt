This module sends out a notification when the site is set to maintenance mode or
set back to live from maintenance.
CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Installation
 * Configuration
 * Maintainers
 
INTRODUCTION
------------

This is very simple module sends out a notification when the site is set to maintenance mode or set back to live from maintenance.

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------
 
 * Configure module on Configration » maintenance Notifications configration form:


 * Configure permission on www.example.com/admin/people/permissions#module-maintenance_notifications


MAINTAINERS
-----------

Current maintainers:
 * Sandeep Reddy (sandeepreddyg) - https://www.drupal.org/u/sandeepreddyg
